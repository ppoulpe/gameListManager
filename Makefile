default: help

help:
	@echo "Usage:"
	@echo "     make [command]"
	@echo
	@echo "Available commands:"
	@grep -v '^_' Makefile | grep '^[^#[:space:]].*:' | grep -v '^default' | sed 's/:\(.*\)//' | xargs -n 1 echo ' -'
	@echo

warm-up:
	docker-compose up -d
	docker exec -it -u dev sf4_php composer self-update --no-interaction
	docker exec -it -u dev sf4_php composer install --no-interaction
	docker exec -it -u dev sf4_php php bin/console doctrine:migrations:migrate --allow-no-migration --no-interaction
	docker exec -it -u dev sf4_php php bin/console assets:install --symlink --relative
	docker exec -it -u dev sf4_php rm -Rf var/cache/
	docker exec -it -u dev sf4_php php bin/console cache:warmup

up:
	docker-compose up -d
	docker-compose exec php rm -Rf var/cache/
    docker exec -it -u dev sf4_php php bin/console cache:warmup

up-with-build:
	docker-compose up -d --build

down:
	docker-compose down

bash:
	docker exec -it -u dev sf4_php bash

cs-fix:
	docker exec -it -u dev sf4_php php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix src
	docker exec -it -u dev sf4_php php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix tests

clear-cache:
	docker exec -it -u dev sf4_php php bin/console cache:clear

make-migration:
	docker exec -it -u dev sf4_php php bin/console make:migration

migrate-migration:
	docker exec -it -u dev sf4_php php bin/console doctrine:migrations:migrate

phpunit:
	docker exec -it -u dev sf4_php vendor/bin/simple-phpunit

composer-install:
	docker exec -it -u dev sf4_php composer install

encore-dev:
	docker exec -it sf4_yarn yarn encore dev

exec-command:
	docker exec -it -u dev sf4_php php $(command)
