$('input#item_name').on('keyup', function () {
    if ($(this).val().length < 3) {
        return false;
    }

    var $this = $(this);

    clearTimeout($.data(this, 'timer'));

    var wait = setTimeout(function () {

        $.ajax({
            url: "https://chicken-coop.p.rapidapi.com/games",
            type: 'GET',
            beforeSend: function (request) {
                request.setRequestHeader("X-RapidAPI-Host", "chicken-coop.p.rapidapi.com");
                request.setRequestHeader("X-RapidAPI-Key", "d8ddfded93mshcc3aa218db21fbap1532b8jsn771fd8799cf3");
            },
            data: {
                title: $this.val()
            }

        }).done(function (result) {
            var divToCustomize = $('div#search-result');
            divToCustomize.empty();
            result.result.forEach(function (elem) {
                divToCustomize.append(
                    "<a href='javascript:void(0);' class='dropdown-item d-flex justify-content-between align-items-center choose_game' data-content='{\"title\": \"" + elem.title + "\", \"platform\": \"" + elem.platform + "\"}'>"+elem.title+"<span class='badge badge-primary badge-pill'>" + elem.platform + "</span></a>"
                );
            });

            $('.dropdown-toggle').dropdown();
        });


    }, 500);

    $(this).data('timer', wait);

});

$("div#search-result").on("click", ".choose_game", function () {

    var jsonValue = JSON.parse($(this).attr('data-content'));

    $('input#item_name').val(jsonValue.title);
    $('input#item_platform').val(jsonValue.platform);

});