<?php

declare(strict_types=1);

namespace App\Presentation\Web\Core\Port\FlashMessage;

interface FlashMessageServiceInterface
{
    public const SUCCESS = 'success';

    public function success(string $message): void;
}
