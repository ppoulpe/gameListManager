<?php

declare(strict_types=1);

namespace App\Presentation\Web\Core\UseCase\User;

use App\Core\Component\User\Application\Service\UserService;
use App\Core\Port\TemplateEngine\TemplateEngineInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfileController extends AbstractController
{
    /**
     * @var TemplateEngineInterface
     */
    private $templateEngine;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(
        TemplateEngineInterface $templateEngine,
        UserService $userService
    ) {
        $this->templateEngine = $templateEngine;
        $this->userService = $userService;
    }

    public function profile()
    {
        return $this->templateEngine->renderResponse('@User/profile.html.twig', ['user' => $this->getUser()]);
    }
}
