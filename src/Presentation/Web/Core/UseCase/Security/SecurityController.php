<?php

namespace App\Presentation\Web\Core\UseCase\Security;

use App\Core\Component\User\Application\Service\UserService;
use App\Core\Component\User\Domain\User;
use App\Core\Port\TemplateEngine\TemplateEngineInterface;
use App\Presentation\Web\Core\Form\Type\RegistrationFormType;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @var TemplateEngineInterface
     */
    private $templateEngine;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(
        TemplateEngineInterface $templateEngine,
        UserService $userService
    ) {
        $this->templateEngine = $templateEngine;
        $this->userService = $userService;
    }

    public function login(AuthenticationUtils $authenticationUtils): ResponseInterface
    {
        return $this->templateEngine->renderResponse(
            '@Security/login.html.twig',
            [
                'last_username' => $authenticationUtils->getLastUsername(),
                'error' => $authenticationUtils->getLastAuthenticationError(),
            ]
        );
    }

    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ): ResponseInterface {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );

            $this->userService->create($user);

            return $this->templateEngine->renderResponse(
                '@Security/login.html.twig'
            );
        }

        return $this->templateEngine->renderResponse(
            '@Security/register.html.twig',
            [
                'registrationForm' => $form->createView(),
            ]
        );
    }
}
