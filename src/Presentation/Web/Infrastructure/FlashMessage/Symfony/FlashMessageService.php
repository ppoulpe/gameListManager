<?php

declare(strict_types=1);

namespace App\Presentation\Web\Infrastructure\FlashMessage\Symfony;

use App\Presentation\Web\Core\Port\FlashMessage\FlashMessageServiceInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

/**
 * Class FlashbagClient.
 */
class FlashMessageService implements FlashMessageServiceInterface
{
    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    public function __construct(FlashBagInterface $flashBag)
    {
        $this->flashBag = $flashBag;
    }

    public function success(string $message): void
    {
        $this->flashBag->add(self::SUCCESS, $message);
    }
}
