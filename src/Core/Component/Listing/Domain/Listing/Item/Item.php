<?php

declare(strict_types=1);

namespace App\Core\Component\Listing\Domain\Listing\Item;

use App\Core\Component\Listing\Domain\Listing\Listing;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Item
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Component\Listing\Domain\Listing\Item\Platform", inversedBy="items")
     */
    private $platform;

    /**
     * @ORM\Column(type="float", scale=2)
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Component\Listing\Domain\Listing\Listing", inversedBy="items")
     */
    private $listing;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @return Item
     */
    public function setName($name): Item
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param $platform
     *
     * @return Item
     */
    public function setPlatform($platform): Item
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @return Listing
     */
    public function getListing(): Listing
    {
        return $this->listing;
    }

    /**
     * @param Listing $listing
     *
     * @return Item
     */
    public function setListing(Listing $listing): Item
    {
        $this->listing = $listing;

        return $this;
    }

    /**
     * @return float
     */
    public function getScore(): float
    {
        return $this->score;
    }

    /**
     * @param float $score
     *
     * @return Item
     */
    public function setScore(float $score): Item
    {
        $this->score = $score;

        return $this;
    }
}
