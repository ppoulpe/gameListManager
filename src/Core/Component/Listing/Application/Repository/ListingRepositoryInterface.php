<?php

declare(strict_types=1);

namespace App\Core\Component\Listing\Application\Repository;

use App\Core\Component\Listing\Domain\Listing\Listing;

interface ListingRepositoryInterface
{
    public function findAll(): array;

    public function find(int $id): ?Listing;

    public function create(Listing $listing): void;

    public function update(Listing $listing): void;

    public function delete(Listing $listing): void;

    public function findByUser($user): array;
}
