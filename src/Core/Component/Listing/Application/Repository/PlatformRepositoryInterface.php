<?php

declare(strict_types=1);

namespace App\Core\Component\Listing\Application\Repository;

use App\Core\Component\Listing\Domain\Listing\Item\Platform;

interface PlatformRepositoryInterface
{
    public function find(?string $id): ?Platform;
}
