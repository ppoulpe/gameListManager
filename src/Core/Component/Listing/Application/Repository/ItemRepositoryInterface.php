<?php

declare(strict_types=1);

namespace App\Core\Component\Listing\Application\Repository;

use App\Core\Component\Listing\Domain\Listing\Item\Item;

interface ItemRepositoryInterface
{
    public function findAll(): array;

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    public function create(Item $item): void;

    public function delete(Item $item): void;
}
