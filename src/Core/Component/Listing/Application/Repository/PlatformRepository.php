<?php

declare(strict_types=1);

namespace App\Core\Component\Listing\Application\Repository;

use App\Core\Component\Listing\Domain\Listing\Item\Platform;
use Doctrine\ORM\EntityManagerInterface;

class PlatformRepository implements PlatformRepositoryInterface
{
    private $entityRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PlatformRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityRepository = $entityManager->getRepository(Platform::class);
    }

    /**
     * @param string $id
     *
     * @return Platform|null
     */
    public function find(?string $id = null): ?Platform
    {
        if (null === $id) {
            return null;
        }

        return $this->entityRepository->find($id);
    }
}
