<?php

declare(strict_types=1);

namespace App\Core\Component\Listing\Application\Service;

use App\Core\Component\Listing\Application\Repository\ListingRepositoryInterface;
use App\Core\Component\Listing\Domain\Listing\Listing;
use App\Presentation\Web\Core\Port\FlashMessage\FlashMessageServiceInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ListingService.
 */
class ListingService
{
    /**
     * @var FlashMessageServiceInterface
     */
    private $flashMessageService;

    /**
     * @var ListingRepositoryInterface
     */
    private $listingRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * ListingService constructor.
     *
     * @param ListingRepositoryInterface   $listingRepository
     * @param FlashMessageServiceInterface $flashMessageService
     * @param TokenStorageInterface        $tokenStorage
     */
    public function __construct(
        ListingRepositoryInterface $listingRepository,
        FlashMessageServiceInterface $flashMessageService,
        TokenStorageInterface $tokenStorage
    ) {
        $this->listingRepository = $listingRepository;
        $this->flashMessageService = $flashMessageService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->listingRepository->findAll();
    }

    /**
     * @return array
     */
    public function findByCurrentUser(): array
    {
        return $this->listingRepository->findByUser(
            $this->tokenStorage->getToken()->getUser()
        );
    }

    /**
     * @param int $id
     *
     * @return Listing
     */
    public function find(int $id): ?Listing
    {
        return $this->listingRepository->find($id);
    }

    /**
     * @param Listing $listing
     */
    public function create(Listing $listing): void
    {
        $listing->setUser(
            $this->tokenStorage->getToken()->getUser()
        );

        $this->listingRepository->create($listing);
        $this->flashMessageService->success('listing.create.success');
    }

    /**
     * @param Listing $listing
     */
    public function update(Listing $listing): void
    {
        $this->listingRepository->update($listing);
        $this->flashMessageService->success('listing.update.success');
    }

    /**
     * @param Listing $listing
     */
    public function delete(Listing $listing): void
    {
        $this->listingRepository->delete($listing);
        $this->flashMessageService->success('listing.delete.success');
    }
}
