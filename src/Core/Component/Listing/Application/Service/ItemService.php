<?php

declare(strict_types=1);

namespace App\Core\Component\Listing\Application\Service;

use App\Core\Component\Listing\Application\Repository\ItemRepositoryInterface;
use App\Core\Component\Listing\Application\Repository\PlatformRepositoryInterface;
use App\Core\Component\Listing\Domain\Listing\Item\Item;
use App\Core\Component\Listing\Domain\Listing\Listing;
use App\Core\SharedKernel\Helper\StringToolTrait;
use App\Presentation\Web\Core\Port\FlashMessage\FlashMessageServiceInterface;
use Unirest\Request;

class ItemService
{
    use StringToolTrait;

    /**
     * @var FlashMessageServiceInterface
     */
    private $flashMessageService;
    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;
    /**
     * @var PlatformRepositoryInterface
     */
    private $platformRepository;

    /**
     * ItemService constructor.
     *
     * @param ItemRepositoryInterface      $itemRepository
     * @param FlashMessageServiceInterface $flashMessageService
     */
    public function __construct(
        ItemRepositoryInterface $itemRepository,
        FlashMessageServiceInterface $flashMessageService,
        PlatformRepositoryInterface $platformRepository
    ) {
        $this->itemRepository = $itemRepository;
        $this->flashMessageService = $flashMessageService;
        $this->platformRepository = $platformRepository;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->itemRepository->findAll();
    }

    /**
     * @param Listing $listing
     * @param Item    $item
     */
    public function create(Listing $listing, Item $item): void
    {
        $item->setListing($listing);

        $response = Request::get('https://chicken-coop.p.rapidapi.com/games/'.$this->format($item->getName()).'?platform=pc',
            [
                'X-RapidAPI-Host' => 'chicken-coop.p.rapidapi.com',
                'X-RapidAPI-Key' => 'd8ddfded93mshcc3aa218db21fbap1532b8jsn771fd8799cf3',
            ]
        );

        $item->setPlatform(
            $this->platformRepository->find($item->getPlatform()) ?? null
        );

        $item->setScore(
            (floatval($response->body->result->score))
        );

        $this->itemRepository->create($item);
        $this->flashMessageService->success('item.post.success');
    }

    /**
     * @param Item $item
     */
    public function delete(Item $item): void
    {
        $this->itemRepository->delete($item);
        $this->flashMessageService->success('item.delete.success');
    }

    /**
     * @param int $listing_id
     *
     * @return array
     */
    public function getItemsByListing(int $listing_id): array
    {
        return $this->itemRepository->findBy([
            'listing' => $listing_id,
        ]);
    }
}
