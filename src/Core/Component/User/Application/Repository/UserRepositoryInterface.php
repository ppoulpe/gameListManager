<?php

declare(strict_types=1);

namespace App\Core\Component\User\Application\Repository;

use App\Core\Component\User\Domain\User;

interface UserRepositoryInterface
{
    public function create(User $item): void;
}
