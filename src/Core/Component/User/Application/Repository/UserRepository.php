<?php

declare(strict_types=1);

namespace App\Core\Component\User\Application\Repository;

use App\Core\Component\User\Domain\User;
use App\Core\Port\Persistence\PersistenceServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PersistenceServiceInterface
     */
    private $persistenceService;

    /**
     * UserRepository constructor.
     *
     * @param EntityManagerInterface      $entityManager
     * @param PersistenceServiceInterface $persistenceService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        PersistenceServiceInterface $persistenceService
    ) {
        $this->entityManager = $entityManager;
        $this->persistenceService = $persistenceService;
    }

    /**
     * @param User $user
     */
    public function create(User $user): void
    {
        $this->persistenceService->upsert($user);
    }
}
