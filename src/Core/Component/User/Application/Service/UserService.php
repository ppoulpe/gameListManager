<?php

declare(strict_types=1);

namespace App\Core\Component\User\Application\Service;

use App\Core\Component\User\Application\Repository\UserRepositoryInterface;
use App\Core\Component\User\Domain\User;

class UserService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserService constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     */
    public function create(User $user): void
    {
        $this->userRepository->create($user);
    }
}
