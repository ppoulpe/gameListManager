<?php

declare(strict_types=1);

namespace App\Core\Port\Response;

use Psr\Http\Message\ResponseInterface;

interface ResponseFactoryInterface
{
    public function redirectToRoute(string $route, array $parameters = [], int $status = 302): ResponseInterface;
}
