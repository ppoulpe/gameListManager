<?php

declare(strict_types=1);

namespace App\Core\Port\TemplateEngine;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface TemplateEngineInterface.
 */
interface TemplateEngineInterface
{
    public function exists(string $template): bool;

    public function render(string $template, array $parameters): string;

    public function renderResponse(string $template, array $parameters = [], ResponseInterface $response = null): ResponseInterface;
}
