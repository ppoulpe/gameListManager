<?php

declare(strict_types=1);

namespace App\Core\Port\Persistence;

/**
 * Interface PersistenceServiceInterface.
 */
interface PersistenceServiceInterface
{
    public function upsert($entity, bool $flush = true): void;

    public function delete($entity, bool $flush = true): void;
}
