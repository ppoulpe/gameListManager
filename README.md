## Game list manager

 Basic list system in PHP.

## Running container

```bash
make up
```

The target `up` on the Makefile creates the containers and starts them. Run `make help` for more information.

