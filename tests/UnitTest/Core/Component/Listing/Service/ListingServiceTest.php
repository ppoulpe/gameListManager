<?php

declare(strict_types=1);

namespace App\tests\UnitTest\Core\Component\Listing\Service;

use App\Core\Component\Listing\Application\Repository\ListingRepository;
use App\Core\Component\Listing\Application\Service\ListingService;
use App\Core\Component\Listing\Domain\Listing\Listing;
use App\Core\Component\User\Domain\User;
use App\Infrastructure\Persistence\Doctrine\PersistenceService;
use App\Presentation\Web\Infrastructure\FlashMessage\Symfony\FlashMessageService;
use App\tests\Framework\AbstractIntegrationTest;
use Mockery;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class ListingServiceTest.
 */
class ListingServiceTest extends AbstractIntegrationTest
{
    private $service;
    private $entityManager;
    private $tokenStorage;
    private $token;

    public function setUp()
    {
        parent::setUp();

        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
        $this->token = Mockery::mock(TokenInterface::class);
        $this->tokenStorage = Mockery::mock(TokenStorageInterface::class);
        $this->service = Mockery::mock(
            ListingService::class,
            [
                new ListingRepository($this->entityManager, new PersistenceService($this->entityManager)),
                new FlashMessageService(new FlashBag()),
                $this->tokenStorage,
            ]
        )->makePartial();
    }

    /**
     * @test
     */
    public function createSuccessfully()
    {
        $listing = new Listing();
        $listing->setId(1);
        $listing->setName('toto');

        $user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['username' => 'username']);

        $this->tokenStorage->shouldReceive('getToken')->andReturn($this->token);
        $this->token->shouldReceive('getUser')->andReturn($user);

        $this->service->create($listing);

        $listings = $this->entityManager
            ->getRepository(Listing::class)
            ->findBy(['name' => 'toto']);

        $this->assertCount(1, $listings);
    }

    /**
     * @test
     */
    public function updateSuccessfully()
    {
        $listing = $this->entityManager
            ->getRepository(Listing::class)
            ->findOneBy(['name' => 'My first listing']);

        $listing->setName('My first listing after update');

        $this->service->update($listing);

        $listings = $this->entityManager
            ->getRepository(Listing::class)
            ->findBy(['name' => 'My first listing after update']);

        $this->assertCount(1, $listings);
    }

    /**
     * @test
     */
    public function deleteSuccessfully()
    {
        $listing = $this->entityManager
            ->getRepository(Listing::class)
            ->findOneBy(['name' => 'My first listing']);

        $this->service->delete($listing);

        $listing = $this->entityManager
            ->getRepository(Listing::class)
            ->findOneBy(['name' => 'My first listing']);

        $this->assertNull($listing);
    }
}
