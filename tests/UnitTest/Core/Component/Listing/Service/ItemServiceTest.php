<?php

declare(strict_types=1);

namespace App\tests\UnitTest\Core\Component\Listing\Service;

use App\Core\Component\Listing\Application\Repository\ItemRepository;
use App\Core\Component\Listing\Application\Repository\PlatformRepository;
use App\Core\Component\Listing\Application\Service\ItemService;
use App\Core\Component\Listing\Domain\Listing\Item\Item;
use App\Core\Component\Listing\Domain\Listing\Listing;
use App\Infrastructure\Persistence\Doctrine\PersistenceService;
use App\Presentation\Web\Infrastructure\FlashMessage\Symfony\FlashMessageService;
use App\tests\Framework\AbstractIntegrationTest;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

/**
 * Class ListingServiceTest.
 */
class ItemServiceTest extends AbstractIntegrationTest
{
    /**
     * @var ItemService
     */
    private $service;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setUp()
    {
        parent::setUp();

        $this->entityManager = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->service = new ItemService(
            new ItemRepository($this->entityManager, new PersistenceService($this->entityManager)),
            new FlashMessageService(new FlashBag()),
            new PlatformRepository($this->entityManager)
        );
    }

    /**
     * @test
     */
    public function getAllSuccessfully()
    {
        $this->assertCount(1, $this->service->getAll());
    }

    /**
     * @test
     */
    public function addSuccessfully()
    {
        $listing = $this->entityManager
            ->getRepository(Listing::class)
            ->findOneBy(['name' => 'My first listing']);

        $item = new Item();
        $item->setName('My awesome item');

        $this->service->create($listing, $item);

        $items = $this->entityManager
            ->getRepository(Item::class)
            ->findBy(['name' => 'My awesome item']);

        $this->assertCount(1, $items);
    }

    /**
     * @test
     */
    public function deleteSuccessfully()
    {
        $item = $this->entityManager
            ->getRepository(Item::class)
            ->findOneBy(['name' => 'My first item']);

        $this->service->delete($item);

        $item = $this->entityManager
            ->getRepository(Item::class)
            ->findOneBy(['name' => 'My first item']);

        $this->assertNull($item);
    }

    /**
     * @test
     */
    public function getItemByListingSuccessfully()
    {
        $listing = $this->entityManager
            ->getRepository(Listing::class)
            ->findOneBy(['name' => 'My second listing']);

        $this->assertCount(1, $this->service->getItemsByListing($listing->getId()));
    }
}
