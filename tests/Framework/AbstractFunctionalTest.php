<?php

declare(strict_types=1);

namespace App\tests\Framework;

use App\Core\Component\User\Domain\User;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Hgraca\DoctrineTestDbRegenerationBundle\EventSubscriber\DatabaseAwareTestInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

/**
 * Class AbstractFunctionalTest.
 */
abstract class AbstractFunctionalTest extends WebTestCase implements DatabaseAwareTestInterface
{
    const PROVIDER_DEFAULT_KEY = 'main';
    const DATA_FIXTURES_FOLDER = __DIR__.'/../DataFixtures';

    /**
     * @var Client
     */
    private $nonStaticClient;

    protected function setUp()
    {
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $loader = new Loader();
        $loader->loadFromDirectory(self::DATA_FIXTURES_FOLDER);

        (new ORMExecutor($em, new ORMPurger()))->execute($loader->getFixtures());
    }

    /**
     * @param array $options
     * @param array $server
     *
     * @return Client|\Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected function getNonStaticClient(array $options = [], array $server = [])
    {
        return $this->nonStaticClient ?? $this->nonStaticClient = parent::createClient($options, $server);
    }

    protected function getContainer(): ContainerInterface
    {
        return $this->getNonStaticClient()->getContainer();
    }

    protected function getEntityManager()
    {
        return $this->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @param $user
     * @param null   $credentials
     * @param string $providerKey
     * @param array  $roles
     */
    protected function login(
        $user,
        $providerKey = self::PROVIDER_DEFAULT_KEY,
        array $roles = [User::ROLE_USER]
    ): void {
        $user = $this->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $user,
            ]);

        $token = new PostAuthenticationGuardToken($user, $providerKey, $roles);

        $session = $this->nonStaticClient->getContainer()->get('session');
        $session->set('_security_'.$providerKey, serialize($token));
        $session->save();

        $this->nonStaticClient->getCookieJar()->set(
            new Cookie($session->getName(), $session->getId())
        );
    }
}
