<?php

declare(strict_types=1);

namespace App\tests\Functionnal\Authentication;

use App\tests\Framework\AbstractFunctionalTest;

class AuthControllerTest extends AbstractFunctionalTest
{
    const PAGE_LOGOUT = '/en/logout';
    const PAGE_LOGIN = '/en/login';
    const PAGE_LIST_ALL = '/en/';

    /**
     * @test
     */
    public function loginSuccessfully()
    {
        $crawler = $this->getNonStaticClient()->request('GET|POST', self::PAGE_LOGIN);

        $form = $crawler->selectButton('Log in!')->form();

        $form['username'] = 'username';
        $form['password'] = 'password';

        $crawler = $this->getNonStaticClient()->submit($form);

        $this->assertContains(
            sprintf('<title>Redirecting to %s</title>', self::PAGE_LIST_ALL),
            $crawler->html()
        );
    }

    /**
     * @test
     */
    public function loginFailure()
    {
        $crawler = $this->getNonStaticClient()->request('GET|POST', self::PAGE_LOGIN);

        $form = $crawler->selectButton('Log in!')->form();

        $form['username'] = 'failure';
        $form['password'] = 'failure';

        $crawler = $this->getNonStaticClient()->submit($form);

        $this->assertContains(
            sprintf('<title>Redirecting to %s</title>', self::PAGE_LOGIN),
            $crawler->html()
        );
    }

    /**
     * @test
     */
    public function logout()
    {
        $this->login('username');
        $this->getNonStaticClient()->request('GET|POST', self::PAGE_LOGOUT);

        $this->assertContains(
            sprintf('<title>Redirecting to %s</title>', self::PAGE_LOGIN),
            $this->getNonStaticClient()->getResponse()->getContent()
        );
    }
}
