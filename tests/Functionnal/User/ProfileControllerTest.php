<?php

declare(strict_types=1);

namespace App\tests\Functionnal\User;

use App\tests\Framework\AbstractFunctionalTest;

class ProfileControllerTest extends AbstractFunctionalTest
{
    const PAGE_PROFILE = '/en/profile';

    /**
     * @test
     */
    public function profile()
    {
        $this->login('username');
        $this->getNonStaticClient()->request('GET|POST', self::PAGE_PROFILE);

        $this->assertContains(
            sprintf('%s&#039;s profile', 'username'),
            $this->getNonStaticClient()->getResponse()->getContent()
        );
    }
}
