<?php

declare(strict_types=1);

namespace App\tests\Functionnal\User;

use App\tests\Framework\AbstractFunctionalTest;

class UserControllerUrlAvailability extends AbstractFunctionalTest
{
    /**
     * @test
     * @dataProvider getProtectedUrls
     */
    public function protectedUrlsAreAvailable(string $url): void
    {
        $this->login('username');
        $this->UrlAreAvailables($url);
    }

    public function getProtectedUrls()
    {
        yield ['/en/profile'];
    }

    private function UrlAreAvailables(string $url): void
    {
        $this->getNonStaticClient()->request(
            'GET|POST',
            sprintf($url, (string) $this->listing->getId())
        );

        $this->assertTrue(
            $this->getNonStaticClient()->getResponse()->isSuccessful()
        );
    }
}
