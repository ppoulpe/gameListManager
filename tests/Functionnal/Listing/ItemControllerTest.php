<?php

declare(strict_types=1);

namespace App\tests\Functionnal\Listing;

use App\Core\Component\Listing\Domain\Listing\Listing;
use App\tests\Framework\AbstractFunctionalTest;
use Symfony\Component\HttpFoundation\Response;

class ItemControllerTest extends AbstractFunctionalTest
{
    const PAGE_CREATE = '/en/list/show/%s/create_item';

    /**
     * @var Listing
     */
    private $listing;

    protected function setUp()
    {
        parent::setUp();

        $this->login('username');

        $this->listing = $this->getEntityManager()
            ->getRepository(Listing::class)
            ->findOneBy([
                'name' => 'My first listing',
            ]);
    }

    /**
     * @test
     */
    public function create()
    {
        $this->login('username');
        $this->getNonStaticClient()->request('GET|POST', self::PAGE_CREATE);

        $this->assertEquals(
            Response::HTTP_OK,
            $this->getNonStaticClient()->getResponse()->getStatusCode()
        );

        $this->assertContains(
            sprintf('<button type="submit" id="item_Save" name="item[Save]" class="btn btn-outline-secondary btn">%s</button>', 'Add an item'),
            $this->getNonStaticClient()->getResponse()->getContent()
        );
    }

    public function delete()
    {
        $this->getNonStaticClient()->request('GET|POST', self::PAGE_CREATE);

        $this->assertEquals(
            Response::HTTP_OK,
            $this->getNonStaticClient()->getResponse()->getStatusCode()
        );

        $this->assertContains(
            '<a class="nav-link" href="/list/create">Create new listing</a>',
            $this->getNonStaticClient()->getResponse()->getContent()
        );
    }
}
