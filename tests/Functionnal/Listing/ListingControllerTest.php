<?php

declare(strict_types=1);

namespace App\tests\Functionnal\Listing;

use App\Core\Component\Listing\Domain\Listing\Listing;
use App\tests\Framework\AbstractFunctionalTest;
use Symfony\Component\HttpFoundation\Response;

class ListingControllerTest extends AbstractFunctionalTest
{
    const PAGE_ALL = '/en/';
    const PAGE_CREATE = '/en/list/create';
    const PAGE_SHOW = '/en/list/show/%s';
    const PAGE_DELETE = '/en/list/delete/%s';

    /**
     * @var Listing
     */
    private $listing;

    protected function setUp()
    {
        parent::setUp();

        $this->login('username');

        $this->listing = $this->getEntityManager()
            ->getRepository(Listing::class)
            ->findOneBy([
                'name' => 'My first listing',
            ]);
    }

    /**
     * @test
     */
    public function all()
    {
        $this->getNonStaticClient()->request('GET|POST', self::PAGE_ALL);

        $this->assertEquals(
            Response::HTTP_OK,
            $this->getNonStaticClient()->getResponse()->getStatusCode()
        );

        $this->assertContains(
            sprintf('<h5 class="card-title">%s</h5>', 'My first listing'),
            $this->getNonStaticClient()->getResponse()->getContent()
        );
    }

    /**
     * @test
     */
    public function create()
    {
        $this->getNonStaticClient()->request('GET|POST', self::PAGE_CREATE);

        $this->assertEquals(
            Response::HTTP_OK,
            $this->getNonStaticClient()->getResponse()->getStatusCode()
        );

        $this->assertContains(
            'Create new listing',
            $this->getNonStaticClient()->getResponse()->getContent()
        );
    }

    /**
     * @test
     */
    public function delete(): void
    {
        $this->getNonStaticClient()->request('GET|POST', sprintf(self::PAGE_DELETE, (string) $this->listing->getId()));

        $this->assertTrue($this->getNonStaticClient()->getResponse()->isRedirection());

        $this->assertContains(
            sprintf('<title>Redirecting to %s</title>', $this->getNonStaticClient()->getResponse()->headers->get('location')),
            $this->getNonStaticClient()->getResponse()->getContent()
        );
    }
}
