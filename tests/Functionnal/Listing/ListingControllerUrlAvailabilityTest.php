<?php

declare(strict_types=1);

namespace App\tests\Functionnal\Listing;

use App\Core\Component\Listing\Domain\Listing\Listing;
use App\tests\Framework\AbstractFunctionalTest;

class ListingControllerUrlAvailabilityTest extends AbstractFunctionalTest
{
    /**
     * @var Listing
     */
    private $listing;

    protected function setUp()
    {
        parent::setUp();

        $this->listing = $this->getEntityManager()
            ->getRepository(Listing::class)
            ->findOneBy([
                'name' => 'My first listing',
            ]);
    }

    /**
     * @test
     * @dataProvider getPublicUrls
     */
    public function publicURLAreAvailable(string $url): void
    {
        $this->UrlAreAvailables($url);
    }

    /**
     * @test
     * @dataProvider getProtectedUrls
     */
    public function protectedUrlsAreAvailable(string $url): void
    {
        $this->login('username');
        $this->UrlAreAvailables($url);
    }

    public function getPublicUrls()
    {
        yield ['/en/login'];
    }

    public function getProtectedUrls()
    {
        yield ['/en/'];
        yield ['/en/list/create'];
    }

    private function UrlAreAvailables(string $url): void
    {
        $this->getNonStaticClient()->request(
            'GET|POST',
            sprintf($url, (string) $this->listing->getId())
        );

        $this->assertTrue(
            $this->getNonStaticClient()->getResponse()->isSuccessful()
        );
    }
}
