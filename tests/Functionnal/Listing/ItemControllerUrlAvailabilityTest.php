<?php

declare(strict_types=1);

namespace App\tests\Functionnal\Listing;

use App\Core\Component\Listing\Domain\Listing\Listing;
use App\tests\Framework\AbstractFunctionalTest;

/**
 * Class ListingControllerTest.
 */
class ItemControllerUrlAvailabilityTest extends AbstractFunctionalTest
{
    /**
     * @var Listing
     */
    private $listingEntity;

    /**
     * @before
     */
    public function load()
    {
        $this->listingEntity = new Listing();
        $this->listingEntity->setId(1);
        $this->listingEntity->setName('My first listing');
    }

    /**
     * @test
     * @dataProvider getProtectedUrls
     */
    public function protectedURLAreAvailable(string $url): void
    {
        $this->login('username');

        $this->getNonStaticClient()->request(
            'GET|POST',
            sprintf($url, (string) $this->listingEntity->getId())
        );

        $this->assertTrue(
            $this->getNonStaticClient()->getResponse()->isSuccessful()
        );
    }

    public function getProtectedUrls()
    {
        yield ['/en/list/show/%s/create_item'];
    }
}
