<?php

declare(strict_types=1);

namespace App\tests\DataFixtures;

use App\Core\Component\Listing\Domain\Listing\Item\Item;
use App\Core\Component\Listing\Domain\Listing\Listing;
use App\Core\Component\User\Domain\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadListingData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('email@provider.com');
        $user->setUsername('username');
        $user->setPassword('$2y$13$TLOGqIW2QJoc388r4a2zuOAG9Eb2MhW78T0Wb5P7vt8ttjUK9i0Eq');

        $listing = new Listing();
        $listing->setName('My first listing');
        $listing->setUser($user);

        $manager->persist($listing);

        $listing = new Listing();
        $listing->setName('My second listing');
        $listing->setUser($user);

        $manager->persist($listing);

        $item = new Item();
        $item->setListing($listing);
        $item->setName('My first item');
        $item->setScore(0);

        $manager->persist($item);

        $manager->flush();
    }
}
